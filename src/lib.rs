
mod shared;
mod frontend;
mod backend;

use std::path::{Path, PathBuf};
use std::ffi::OsStr;
use std::collections::{HashSet, HashMap};

use derive_more::Display;
use log::{error, debug};

use crate::shared::error;
use crate::shared::intermediate::Intermediate;

pub struct CompilerOptions<'a> {
    pub input_file_path: &'a Path,
    pub input_file_type: Option<&'a str>,
    pub optimization_level: u32,
    pub count_statements: Option<&'a str>,
    pub user_flags: &'a HashMap<String, bool>,
    pub frontend_only: bool,
    pub backend_name: &'a str,
    pub backend_args: Vec<String>,
    pub output_file: Option<&'a str>,
}

pub struct Options<'a> {
    pub flags: HashSet<String>,
    pub backend_args: &'a Vec<String>,
}

const FRONTENDS: [(&'static str, fn(&Path, &Options) -> frontend::Result); 1] = [
    ("hdl", frontend::compile),
];

const BACKENDS: [(&'static str, fn(Option<&str>, &Intermediate, &Options) -> backend::Result); 1] = [
    ("LogicSimulator", backend::logic_simulator::LogicSimulator::run),
];

const INPUT_FILE_TYPES: [(&'static str, &str); 2] = [
    ("hdl", "hdl"),
    ("hdli", "intermediate"),
];

const OPTIMIZATION_FLAGS: &[(&'static str, u32)] = &[
    // (flag name, minimum optimization level)
    ("constant-folding", 0),
    ("redundant-signals", 1),
];

const FLAGS: &[&'static str] = &[
    "dump-intermediate",
    "warn-unused-locals",
    "warn-unassigned-locals",
];

const DEFAULT_FLAGS: &[&'static str] = &[
    "warn-unused-locals",
    "warn-unassigned-locals",
];

#[derive(Debug, Display)]
pub enum ErrorKind {
    #[display(fmt = "failed to compile {}", "_0.display()")]
    Compile(PathBuf),
    #[display(fmt = "error in backend")]
    Backend,
    #[display(fmt = "{}: file format not recognized", "_0.display()")]
    FileFormatNotRecognized(PathBuf),
    #[display(fmt = "feature not implemented yet")]
    NotImplemented,
}

pub type Error = error::Error<ErrorKind>;

pub fn available_frontends() -> Vec<&'static str> {
    FRONTENDS.iter()
        .map(|frontend| frontend.0)
        .collect::<Vec<_>>()
}

pub fn available_backends() -> (Vec<&'static str>, &'static str) {
    let backends = BACKENDS.iter()
        .map(|backend| backend.0)
        .collect::<Vec<_>>();
    let default = backends[0];
    (backends, default)
}

pub fn available_flags() -> Vec<&'static str> {
    let mut flags = OPTIMIZATION_FLAGS.iter()
        .map(|backend| backend.0)
        .collect::<Vec<_>>();

    flags.extend(FLAGS);

    flags.sort_unstable();

    flags
}

pub fn run(compiler_options: &CompilerOptions) -> Result<(), Error> {
    let mut flags: HashSet<String> = DEFAULT_FLAGS.iter()
        .map(|flag| flag.to_string())
        .collect();
    for (name, min_level) in OPTIMIZATION_FLAGS {
        if *min_level <= compiler_options.optimization_level {
            flags.insert(name.to_string());
        }
    }
    for (name, value) in compiler_options.user_flags {
        if *value {
            flags.insert(name.to_string());
        } else {
            flags.remove(name);
        }
    }

    if log::log_enabled!(log::Level::Debug) {
        let flags_str = flags.iter()
            .cloned()
            .collect::<Vec<_>>()
            .join(", ");
        debug!("compilation flags: {}", flags_str);
    }

    let options = Options {
        flags,
        backend_args: &compiler_options.backend_args,
    };

    let frontend_name = if let Some(value) = compiler_options.input_file_type {
        value
    } else {
        compiler_options.input_file_path.extension()
            .and_then(OsStr::to_str)
            .and_then(|ext| {
                INPUT_FILE_TYPES.iter()
                    .find_map(|file_type| if file_type.0 == ext {
                        Some(file_type.1)
                    } else {
                        None
                    })
            })
            .ok_or_else(|| ErrorKind::FileFormatNotRecognized(compiler_options.input_file_path.to_path_buf()))?
    };

    let frontend_fn = FRONTENDS.iter()
        .find(|frontend| frontend.0 == frontend_name)
        .unwrap().1;

    let intermediate = frontend_fn(compiler_options.input_file_path, &options)
        .map_err(|err| Error::with_source(ErrorKind::Compile(compiler_options.input_file_path.to_path_buf()), err))?;

    if options.flags.contains("dump-intermediate") {
        for block in &intermediate {
            println!("{}", block);
        }
    }

    if let Some(block_name) = compiler_options.count_statements {
        let block = intermediate.iter()
            .find(|block| block.name == block_name);

        if let Some(block) = block {
            println!("{}", block.count_statements());
        } else {
            error!("block \"{}\" not found", block_name);
        }
    }

    if compiler_options.frontend_only {
        // TODO: output intermediate file
        return Err(ErrorKind::NotImplemented.into());
    } else {
        let backend_fn = BACKENDS.iter()
            .find(|backend| backend.0 == compiler_options.backend_name)
            .unwrap().1;

        backend_fn(compiler_options.output_file, &intermediate, &options)
            .map_err(|err| Error::with_source(ErrorKind::Backend, err))?;
    }

    Ok(())
}
