
use crate::frontend::ast::{ExpressionNode, ExpressionNodeData, BehaviourStatementNode, UnaryOp, NumberNode, BinaryOp, BlockNode, RootNode};
use std::rc::Rc;
use std::cell::RefCell;

pub fn optimize(root: &mut RootNode) -> bool {
    optimize_root(root)
}

fn optimize_root(root: &mut RootNode) -> bool {
    let mut changed = false;

    for block in &root.blocks {
        let block_changed = optimize_block(block);

        if block_changed {
            changed = true;
        }
    }

    changed
}

fn optimize_block(block: &Rc<RefCell<BlockNode>>) -> bool {
    let mut changed = false;

    let mut block_ref = block.borrow_mut();

    for behaviour_statement in &mut block_ref.behaviour_statements {
        let statement_changed = optimize_behaviour_statement(behaviour_statement);

        if statement_changed {
            changed = true;
        }
    }

    changed
}

fn optimize_behaviour_statement(behaviour_statement: &mut BehaviourStatementNode) -> bool {
    let source_changed = optimize_expression(behaviour_statement.source.as_mut());

    // match behaviour_statement.source.data {
    //     ExpressionNodeData::Const(number) => {
    //         // TODO: this is not always possible (i.e. multiple assignments)
    //         // TODO: only if different, set changed
    //         // behaviour_statement.target.constant_value = Some(number);
    //     },
    //     _ => {}
    // }

    source_changed
}

fn optimize_expression(expression: &mut ExpressionNode) -> bool {
    let (changed, new_const_value) = match &mut expression.data {
        ExpressionNodeData::Binary(op, left, right) =>
            optimize_binary_expression(*op, left, right),
        ExpressionNodeData::Unary(op, operand) =>
            optimize_unary_expression(*op, operand),
        ExpressionNodeData::Variable(_) => (false, None),
        ExpressionNodeData::Const(_) => (false, None),
    };

    if let Some(new_const_value) = new_const_value {
        *expression = ExpressionNode {
            data: ExpressionNodeData::Const(Box::new(NumberNode {
                value: new_const_value,
                width: Some(expression.typ.as_ref().unwrap().width),
            })),
            typ: expression.typ.clone(),
        };
    }

    changed
}

fn optimize_unary_expression(op: UnaryOp, operand: &mut ExpressionNode) -> (bool, Option<u64>) {
    let operand_changed = optimize_expression(operand);

    let new_const_value = if let Some(operand_value) = operand.const_value() {
        match op {
            UnaryOp::NOT => Some(!operand_value.value),
        }
    } else {
        None
    };

    (operand_changed | new_const_value.is_some(), new_const_value)
}

fn optimize_binary_expression(op: BinaryOp, left: &mut ExpressionNode, right: &mut ExpressionNode) -> (bool, Option<u64>) {
    let left_changed = optimize_expression(left);
    let right_changed = optimize_expression(right);

    let left_value = left.const_value();
    let right_value = right.const_value();

    let new_const_value = if let (Some(left_value), Some(right_value)) = (left_value, right_value) {
        match op {
            BinaryOp::AND => Some(left_value.value & right_value.value),
            BinaryOp::OR => Some(left_value.value | right_value.value),
            BinaryOp::XOR => Some(left_value.value ^ right_value.value),
            BinaryOp::Add => Some(left_value.value + right_value.value),
            BinaryOp::Concatenate => Some(left_value.value << right_value.width.unwrap() | right_value.value),
        }
    } else if let Some(left_value) = left_value {
        // TODO: eliminate `0 | x`, `1 & x`, `0 + x`, ...
        //  requires read/write access count
        None
    } else if let Some(right_value) = right_value {
        // TODO
        None
    } else {
        None
    };

    (left_changed | right_changed | new_const_value.is_some(), new_const_value)
}