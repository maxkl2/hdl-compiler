
use log::{Metadata, Record, SetLoggerError, LevelFilter, Level};
use colored::Colorize;

fn verbosity_to_level_filter(verbosity: u32) -> LevelFilter {
    match verbosity {
        0 => LevelFilter::Error,
        1 => LevelFilter::Warn,
        2 => LevelFilter::Info,
        3 => LevelFilter::Debug,
        _ => LevelFilter::Trace,
    }
}

pub struct Logger {}

impl Logger {
    pub fn init(verbosity: u32) -> Result<(), SetLoggerError> {
        let logger = Box::new(Logger {});

        let result = log::set_boxed_logger(logger);

        if result.is_ok() {
            log::set_max_level(verbosity_to_level_filter(verbosity));
        }

        result
    }
}

impl log::Log for Logger {
    fn enabled(&self, _metadata: &Metadata) -> bool {
        true
    }

    fn log(&self, record: &Record) {
        if self.enabled(record.metadata()) {
            let prefix = match record.level() {
                Level::Error => "error: ".bold().red(),
                Level::Warn => "warning: ".bold().yellow(),
                Level::Info => "info: ".bold().blue(),
                Level::Debug => "".into(),
                Level::Trace => "".into(),
            };
            eprintln!("{}{}", prefix, record.args());
        }
    }

    fn flush(&self) {}
}