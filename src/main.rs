
mod logger;

use std::error::Error;
use std::env;
use std::path::Path;
use std::collections::HashMap;

use clap::{App, Arg, crate_name, crate_version, crate_authors, crate_description};
use log::{error, debug};

use hdl_compiler::CompilerOptions;
use logger::Logger;

fn main() {
    let frontend_names = hdl_compiler::available_frontends();
    let (backend_names, default_backend) = hdl_compiler::available_backends();
    let available_flags = hdl_compiler::available_flags();

    let matches = App::new(crate_name!())
        .version(crate_version!())
        .author(crate_authors!())
        .about(crate_description!())
        .arg(Arg::with_name("input_file_type")
            .short("t")
            .value_name("TYPE")
            .help("Treat the input file as having type TYPE. If not specified, the type is guessed based on the file extension")
            .possible_values(&frontend_names))
        .arg(Arg::with_name("dump_intermediate")
            .short("d")
            .help("Pretty-print the intermediate code to the standard output after running the frontend"))
        .arg(Arg::with_name("frontend_only")
            .short("i")
            .help("Compile the input file to intermediate code"))
        .arg(Arg::with_name("backend")
            .short("b")
            .value_name("BACKEND")
            .help("Use a specific backend")
            .possible_values(&backend_names)
            .default_value(default_backend))
        .arg(Arg::with_name("output_file")
            .short("o")
            .value_name("FILE")
            .help("Write output to FILE"))
        .arg(Arg::with_name("optimization")
            .short("O")
            .value_name("LEVEL")
            .help("Optimization level")
            .possible_values(&["0", "1"])
            .default_value("1"))
        .arg(Arg::with_name("flags")
            .short("f")
            .value_name("FLAG")
            .help(&("Set a flag. Prepending 'no-' to a flag disables it, overriding the default value. Pass '--help' to see available flags."))
            .long_help(&("Set a flag. Prepending 'no-' to a flag disables it, overriding the default value.\nAvailable flags are: ".to_string() + &available_flags.join(", ")))
            .multiple(true)
            .number_of_values(1))
        .arg(Arg::with_name("count_statements")
            .long("count-statements")
            .value_name("BLOCK")
            .help("Print the number of intermediate statements in the specified block"))
        .arg(Arg::with_name("backend_args")
            .short("B")
            .value_name("ARG")
            .help("Pass comma-separated list of arguments to the backend")
            .multiple(true)
            .require_delimiter(true)
            .require_equals(true))
        .arg(Arg::with_name("verbosity")
            .short("v")
            .multiple(true)
            .help("Increase logging verbosity"))
        .arg(Arg::with_name("color")
            .long("color")
            .takes_value(true)
            .help("Enable or disable colored output")
            .possible_values(&["always", "never", "auto"])
            .default_value("auto"))
        .arg(Arg::with_name("input_file")
            .value_name("FILE")
            .help("The input file")
            .required(true))
        .get_matches();

    let verbosity = matches.occurrences_of("verbosity");

    let color = matches.value_of("color");
    if let Some(color) = color {
        match color {
            "always" => colored::control::set_override(true),
            "never" => colored::control::set_override(false),
            "auto" => colored::control::unset_override(),
            _ => unreachable!(),
        }
    }

    Logger::init(verbosity as u32)
        .expect("Failed to set up logger");

    debug!("{} {}", crate_name!(), crate_version!());

    let frontend_only = matches.is_present("frontend_only");

    let backend_name = matches.value_of("backend").unwrap();

    let optimization_level = matches.value_of("optimization").unwrap()
        .parse::<u32>()
        .unwrap();

    let flags_raw = matches.values_of("flags")
        .map(|flags| flags.collect::<Vec<_>>())
        .unwrap_or(Vec::new());
    let mut user_flags = HashMap::new();
    for flag in flags_raw {
        if flag.starts_with("no-") {
            let without_no = &flag["no-".len()..];
            user_flags.insert(without_no.to_string(), false);
        } else {
            user_flags.insert(flag.to_string(), true);
        }
    }

    let count_statements = matches.value_of("count_statements");

    let output_file = matches.value_of("output_file");

    let input_file_name = matches.value_of("input_file").unwrap();
    let input_file_type = matches.value_of("input_file_type");

    let backend_args = matches.values_of("backend_args")
        .map(|backend_args| backend_args.map(str::to_string).collect::<Vec<_>>())
        .unwrap_or(Vec::new());

    let input_file_path = Path::new(input_file_name);

    let options = CompilerOptions {
        input_file_path,
        input_file_type,
        optimization_level,
        count_statements,
        user_flags: &user_flags,
        frontend_only,
        backend_name,
        backend_args,
        output_file,
    };

    if let Err(err) = hdl_compiler::run(&options) {
        error!("{}", err);

        let mut err: &dyn Error = &err;
        while let Some(source) = err.source() {
            error!("caused by: {}", source);
            err = source;
        }
    }
}
